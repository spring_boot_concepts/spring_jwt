package com.spring_jwt.example.filter;

import com.spring_jwt.example.service.MyUserDetailsService;
import com.spring_jwt.example.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilt extends OncePerRequestFilter {

    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    MyUserDetailsService myUserDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String authoriationHeader=httpServletRequest.getHeader("Authorization");
        String userName=null;
        String jwt=null;
        if(authoriationHeader!=null && authoriationHeader.startsWith("Bearer ")){
            jwt=authoriationHeader.substring(7);
            userName=jwtUtil.extractUsername(jwt);
        }
        if(userName!=null && SecurityContextHolder.getContext().getAuthentication()==null){
           UserDetails userDetails=myUserDetailsService.loadUserByUsername(userName);
           if(jwtUtil.isvalidToken(jwt,userDetails)){
               UsernamePasswordAuthenticationToken usernamePwdAuth=
                       new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
               usernamePwdAuth.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
               SecurityContextHolder.getContext().setAuthentication(usernamePwdAuth);
           }
        }
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}
